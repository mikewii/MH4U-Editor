#include <wx/wx.h>
#include "Tools/Buffer.hpp"
#include "icon.png.h"
#include "GUI/Parsers/MHToolsMainFrame.h"

class MyApp : public wxApp
{
public:
    virtual bool OnInit(void) override
    {
        MHToolsMainFrame *mainWindow = new MHToolsMainFrame(NULL);


        wxImage::AddHandler(new wxPNGHandler);
        wxIcon icon;
        icon.CopyFromBitmap(icon_png_to_wx_bitmap());
        mainWindow->SetIcon(icon);
        mainWindow->Show();
        return true;
    }
};

wxIMPLEMENT_APP_NO_MAIN(MyApp);

int main(int argc, char *argv[])
{
    wxEntry(argc, argv);

    wxEntryCleanup();

    TOOLS::Buffer::Cleanup();

    return 0;
}
